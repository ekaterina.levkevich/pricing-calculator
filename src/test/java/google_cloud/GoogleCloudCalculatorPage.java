package google_cloud;

import org.openqa.selenium.WebDriver;

public class GoogleCloudCalculatorPage {
    private WebDriver driver;
    private String addToEstimateButtonXPath = "//span[@jsname='m9ZlFb']/ancestor::button[1]";
    public GoogleCloudCalculatorPage(WebDriver driver){
        this.driver = driver;
    }

    public void addToEstimate() {
        PageUtils.waitToBeClickable(addToEstimateButtonXPath, driver).click();
    }

    public void chooseTool(String toolName) {
        String toolNameButtonXPath = "//h2[text()='" + toolName + "']/ancestor::div[1]";
        PageUtils.waitToBeClickable(toolNameButtonXPath, driver).click();
    }

}
