package google_cloud;

import org.openqa.selenium.WebDriver;


public class ComputeEnginePage {
    private WebDriver driver;
    private String numberOfInstanceFieldXPath = "//div[@jsname='nd797b']/descendant::input[@value]";
    private String operatingSystemFieldXPath = "//span[text()='Operating System / Software']/ancestor::div[@jsname='oYxtQd']";
    private String machineFamilyFieldXPath = "//span[text()='Machine Family']/ancestor::div[@jsname='oYxtQd']";
    private String seriesFieldXPath = "//span[text()='Series']/ancestor::div[@jsname='oYxtQd']";
    private String machineTypeFieldXPath = "//span[text()='Machine type']/ancestor::div[@jsname='oYxtQd']";
    private String addGPUsButtonXPath = "//button[@aria-label='Add GPUs']";
    private String gpuModelFieldXPath = "//span[text()='GPU Model']/ancestor::div[@jsname='oYxtQd']";
    private String numberOfGPUsFieldXPath = "//span[text()='Number of GPUs']/ancestor::div[@jsname='oYxtQd']";
    private String localSSDFieldXPath = "//span[text()='Local SSD']/ancestor::div[@jsname='oYxtQd']";
    private String datacenterLocationFieldXPath = "//span[text()='Region']/ancestor::div[@jsname='oYxtQd']";
    private String estimatedCostXPath = "//div[text()='Estimated cost']/following::label";
    private String shareButtonXPath = "//button[@aria-label='Open Share Estimate dialog']";

    public ComputeEnginePage(WebDriver driver){
        this.driver = driver;
    }

    public void setNumberOfInstance(String numberOfInstance) {
        PageUtils.waitToBeClickable(numberOfInstanceFieldXPath, driver).clear();
        PageUtils.getElementByXPath(numberOfInstanceFieldXPath, driver).sendKeys(numberOfInstance);
    }

    public void setOperatingSystem(String operatingSystem) {
        String operatingSystemVariantXPath =
                "//span[text()='" + operatingSystem +"']/ancestor::li";
        PageUtils.waitToBeClickable(operatingSystemFieldXPath, driver).click();
        PageUtils.waitToBeClickable(operatingSystemVariantXPath, driver).click();
    }

    public void selectProvisioningModel(String provisioningModel) {
        String provisioningModelXPath = "//label[text()='" + provisioningModel + "']/parent::div";
        PageUtils.getElementByXPath(provisioningModelXPath, driver).click();
    }

    public void setMachineFamily(String machineFamilyType) {
        String machineFamilyTypeXPath = "//span[text()='" + machineFamilyType + "']/ancestor::li";
        PageUtils.getElementByXPath(machineFamilyFieldXPath, driver).click();
        PageUtils.getElementByXPath(machineFamilyTypeXPath, driver).click();
    }

    public void setSeries(String series) {
        String seriesTypeXPath = "//span[text()='" + series + "']/ancestor::li";
        PageUtils.getElementByXPath(seriesFieldXPath, driver).click();
        PageUtils.getElementByXPath(seriesTypeXPath, driver).click();
    }

    public void setMachineType(String machineType) {
        String machineTypeXPath = "//span[text()='" + machineType + "']/ancestor::li";
        PageUtils.getElementByXPath(machineTypeFieldXPath, driver).click();
        PageUtils.getElementByXPath(machineTypeXPath, driver).click();
    }

    //This button turn off by default
    public void switchAddGPUsButtonState() {
        PageUtils.getElementByXPath(addGPUsButtonXPath, driver).click();
    }

    public void setGPUType(String gpuType) {
        String gpuTypeXPath = "//span[text()='" + gpuType + "']/ancestor::li";
        PageUtils.waitToBeClickable(gpuModelFieldXPath, driver).click();
        PageUtils.getElementByXPath(gpuTypeXPath, driver).click();
    }

    public void setNumberOfGPUs(String numberOfGPUs) {
        String numberOfGPUsXPath = "//span[text()='" + numberOfGPUs + "']/ancestor::li";
        PageUtils.getElementByXPath(numberOfGPUsFieldXPath, driver).click();
        PageUtils.getElementByXPath(numberOfGPUsXPath, driver).click();
    }

    public void setLocalSSD(String localSSD) {
        String localSSDXPath = "//span[text()='" + localSSD + "']/ancestor::li";
        PageUtils.getElementByXPath(localSSDFieldXPath, driver).click();
        PageUtils.getElementByXPath(localSSDXPath, driver).click();
    }

    public void setDatacenterLocation(String datacenterLocation) {
        String datacenterLocationXPath = "//span[text()='" + datacenterLocation + "']/ancestor::li";
        PageUtils.getElementByXPath(datacenterLocationFieldXPath, driver).click();
        PageUtils.getElementByXPath(datacenterLocationXPath, driver).click();
    }

    public void selectCommittedUsage(String committedUsage) {
        String committedUsageXPath = "//label[text()='" + committedUsage + "']/parent::div";
        PageUtils.getElementByXPath(committedUsageXPath, driver).click();
    }

    public String getEstimatedCost() throws InterruptedException {
        /*Of course, using 'Thread.sleep' is a bad practice, but here it is the best solution*/
        Thread.sleep(3000);
        return PageUtils.getElementByXPath(estimatedCostXPath, driver).getText();
    }

    public void clickShareButton() {
        PageUtils.waitToBeClickable(shareButtonXPath, driver).click();
    }

    public String getOpenEstimateSummaryLink() {
        String openEstimateSummaryLinkXPath = "//a[@track-name='open estimate summary']";
        return PageUtils.getElementByXPath(openEstimateSummaryLinkXPath, driver).getAttribute("href");
    }
}
