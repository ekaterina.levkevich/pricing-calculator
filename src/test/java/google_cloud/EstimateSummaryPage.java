package google_cloud;

import org.openqa.selenium.WebDriver;

public class EstimateSummaryPage {
    WebDriver driver;
    String totalEstimateCostXPath = "//h5[text() = 'Total estimated cost']/following-sibling::h4";

    public EstimateSummaryPage(WebDriver driver, String estimateSummaryLink) {
        this.driver = driver;
        driver.get(estimateSummaryLink);
    }

    public String getTotalEstimatedCost() {
        return PageUtils.waitToBeClickable(totalEstimateCostXPath, driver).getText();
    }
}
