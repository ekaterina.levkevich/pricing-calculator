package google_cloud;

import org.openqa.selenium.WebDriver;

public class SearchResultPage {
    private WebDriver driver;
    private String calculatorPageLinkPath = "//a[@href='https://cloud.google.com/products/calculator']";
    public SearchResultPage(WebDriver driver){
        this.driver = driver;
    }

    public void goToCalculatorPage() {
        PageUtils.waitToBeClickable(calculatorPageLinkPath, driver).click();
    }
}
