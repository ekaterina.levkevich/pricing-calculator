package google_cloud;

import org.openqa.selenium.WebDriver;

public class GoogleCloudHomePage {
    private WebDriver driver;
    private String searchFieldXPath = "//form/descendant::input[@type='text']";
    private String searchButtonXPath = "//form/descendant::i[@aria-label='Search']";

    public GoogleCloudHomePage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://cloud.google.com/");
    }

    public void searchIconClick() {
        PageUtils.waitToBeClickable("//div[@class='YSM5S']", driver).click();
    }

    public void setSearchField(String searchFieldFilling)  {
        PageUtils.getElementByXPath(searchFieldXPath, driver).sendKeys(searchFieldFilling);
    }

    public void search() {
        PageUtils.getElementByXPath(searchButtonXPath, driver).click();
    }
}
