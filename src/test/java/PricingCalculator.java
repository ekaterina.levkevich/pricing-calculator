import google_cloud.*;
import yop_mail.RandomGeneratedEmailPage;
import yop_mail.YopMailMainPage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PricingCalculator {
    @Test
    public void computeEngineInstanceCreation() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        GoogleCloudHomePage googleCloudHomePage = new GoogleCloudHomePage(driver);
        googleCloudHomePage.searchIconClick();
        googleCloudHomePage.setSearchField("Google Cloud Platform Pricing Calculator");
        googleCloudHomePage.search();

        SearchResultPage searchResultPage = new SearchResultPage(driver);
        searchResultPage.goToCalculatorPage();

        /*Due to absence of the 'COMPUTE ENGINE' on the current page the order of steps has been changed*/
        GoogleCloudCalculatorPage googleCloudCalculatorPage = new GoogleCloudCalculatorPage(driver);
        /*First 'Add to estimate' button press*/
        googleCloudCalculatorPage.addToEstimate();
        /*Then 'Compute Engine' select*/
        googleCloudCalculatorPage.chooseTool("Compute Engine");

        ComputeEnginePage computeEnginePage = new ComputeEnginePage(driver);
        computeEnginePage.setNumberOfInstance("4");
        /*Since there is no 'What are these instances for?', appropriate step is missing.
        * Due to absence the variant 'Free: Debian, CentOS, CoreOS, Ubuntu, or another User-Provided OS',
        * the variant 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)' was selected
        */
        computeEnginePage.setOperatingSystem("Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)");
        computeEnginePage.selectProvisioningModel("Regular");
        computeEnginePage.setMachineFamily("General Purpose");
        computeEnginePage.setSeries("N1");
        computeEnginePage.setMachineType("n1-standard-8");
        computeEnginePage.switchAddGPUsButtonState();
        computeEnginePage.setGPUType("NVIDIA Tesla V100");
        computeEnginePage.setNumberOfGPUs("1");
        computeEnginePage.setLocalSSD("2x375 GB");
        /*Due to absence the variant 'Frankfurt (europe-west3)' in the drop-down list,
        * the 'Netherlands (europe-west4)' was selected
        */
        computeEnginePage.setDatacenterLocation("Netherlands (europe-west4)");
        computeEnginePage.selectCommittedUsage("1 year");
        String estimatedCostOnComputeEnginePage = computeEnginePage.getEstimatedCost();
        /*$138.70 - the cost by default*/
        assertNotEquals("$138.70", computeEnginePage.getEstimatedCost());
        /*Since 'SEND EMAIL' button absent, the following steps have been replaced with steps from the task for Framework:
         * 9. Click "Share" to see Total estimated cost.
         * 10. Click "Open estimate summary" to see Cost Estimate Summary, will be opened in separate tab browser.
         * 11. Verify that the 'Cost Estimate Summary' matches with filled values in Step 8.
         */
        computeEnginePage.clickShareButton();
        String estimateSummaryLink = computeEnginePage.getOpenEstimateSummaryLink();

        WebDriver newDriver = new ChromeDriver();
        EstimateSummaryPage estimateSummaryPage = new EstimateSummaryPage(newDriver, estimateSummaryLink);
        assertEquals(estimatedCostOnComputeEnginePage, estimateSummaryPage.getTotalEstimatedCost());
        driver.quit();
        newDriver.quit();
    }

    /*For random mail generation part (steps 10-12) was created separate test*/
    @Test
    public void randomMailCreation() {
        WebDriver driver = new ChromeDriver();
        YopMailMainPage yopMailMainPage = new YopMailMainPage(driver);
        yopMailMainPage.generateRandomEmail();

        RandomGeneratedEmailPage randomEmailPage = new RandomGeneratedEmailPage(driver);
        String generatedEmail = randomEmailPage.getGeneratedEmail();
        System.out.println(generatedEmail);
        driver.quit();
    }
}
