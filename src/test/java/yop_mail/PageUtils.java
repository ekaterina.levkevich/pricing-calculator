package yop_mail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class PageUtils {
    public static WebElement getElementByXPath(String xPath, WebDriver driver) {
        return driver.findElement(new By.ByXPath(xPath));
    }

    public static WebElement waitToBeClickable(String xPath, WebDriver driver) {
        By locator = new By.ByXPath(xPath);
        ExpectedCondition<WebElement> condition = ExpectedConditions.elementToBeClickable(locator);
        return new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS)).until(condition);
    }

    public static WebElement waitVisibilityOf(String xPath, WebDriver driver) {
        By locator = new By.ByXPath(xPath);
        ExpectedCondition<WebElement> condition = ExpectedConditions.visibilityOfElementLocated(locator);
        return new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS)).until(condition);
    }

}
