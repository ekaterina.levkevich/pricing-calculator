package yop_mail;

import org.openqa.selenium.WebDriver;

public class RandomGeneratedEmailPage {
    WebDriver driver;
    private String generateEmailXPath = "//span[@class='genytxt']";

    public RandomGeneratedEmailPage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://yopmail.com/en/email-generator");
    }

    public String getGeneratedEmail() {
        String login = PageUtils.waitVisibilityOf(generateEmailXPath, driver).getText();
        login.replace("\"", "");
        return login + "@yopmail.com";
    }
}
