package yop_mail;

import org.openqa.selenium.WebDriver;

public class YopMailMainPage {
    private String randomEmailGeneratorButtonXPath = "//h3[text()='Random Email generator']/ancestor::a";
    private WebDriver driver;
    public YopMailMainPage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://yopmail.com/en/");
    }

    public void generateRandomEmail() {
        PageUtils.waitToBeClickable(randomEmailGeneratorButtonXPath, driver).click();
    }
}
